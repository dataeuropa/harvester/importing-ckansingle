package io.piveau.importing.ckan.utils;

import io.piveau.pipe.PipeContext;
import io.piveau.utils.DigestAuth;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFactory;
import org.apache.jena.vocabulary.DCAT;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

public class SparqlConnector {

    private final WebClient client;

    private final PipeContext pipeContext;

    public static SparqlConnector create(WebClient client, PipeContext pipeContext) {
        return new SparqlConnector(client, pipeContext);
    }

    private SparqlConnector(WebClient client, PipeContext pipeContext) {
        this.client = client;
        this.pipeContext = pipeContext;
    }

    public Future<Integer> getTotalCount() {
        String query = "SELECT (COUNT(DISTINCT ?s) AS ?count) WHERE { GRAPH ?g { ?s a <" + DCAT.Dataset + "> } }";

        Promise<Integer> promise = Promise.promise();

        HttpRequest<Buffer> request = client.getAbs(pipeContext.getConfig().getString("sparqlEndpoint"))
                .addQueryParam("default-graph-uri", "")
                .addQueryParam("query", query)
                .putHeader("Accept", "application/json");
        query(request, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                ResultSet result = ResultSetFactory.fromJSON(new ByteArrayInputStream(response.bodyAsString().getBytes()));
                Integer totalCount = result.next().getLiteral("count").getInt();
                promise.complete(totalCount);
            } else {
                promise.fail(ar.cause());
            }
        });
        return promise.future();
    }

    public Future<List<String>> getIdentifierPage(int offset, int pageSize) {
        String query = "SELECT ?s WHERE { GRAPH ?g { ?s a <" + DCAT.Dataset + "> } } OFFSET " + offset + " LIMIT " + pageSize;

        Promise<List<String>> promise = Promise.promise();

        HttpRequest<Buffer> request = client.getAbs(pipeContext.getConfig().getString("sparqlEndpoint"))
                .addQueryParam("default-graph-uri", "")
                .addQueryParam("query", query)
                .putHeader("Accept", "application/json");
        query(request, ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                ResultSet result = ResultSetFactory.fromJSON(new ByteArrayInputStream(response.body().getBytes()));
                List<String> datasets = new ArrayList<>();
                while (result.hasNext()) {
                    datasets.add(result.next().getResource("s").getURI());
                }
                promise.complete(datasets);
            } else {
                promise.fail(ar.cause());
            }
        });
        return promise.future();
    }

    private void query(HttpRequest<Buffer> request, Handler<AsyncResult<HttpResponse<Buffer>>> handler) {
        request.send(ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                if (response.statusCode() == 401) {
                    String username = pipeContext.getConfig().getString("username");
                    String password = pipeContext.getConfig().getString("password");
                    String authenticate = DigestAuth.authenticate(response.getHeader("WWW-Authenticate"), pipeContext.getConfig().getString("sparqlEndpoint"), HttpMethod.GET.name(), username, password);
                    if (authenticate != null) {
                        request.putHeader("Authorization", authenticate);
                        query(request, handler);
                    } else {
                        handler.handle(Future.failedFuture("Could not authenticate"));
                    }
                } else if (response.statusCode() == 200 || response.statusCode() == 201 || response.statusCode() == 204) {
                    handler.handle(Future.succeededFuture(response));
                } else {
                    handler.handle(Future.failedFuture(response.statusMessage()));
                }
            } else {
                handler.handle(Future.failedFuture(ar.cause()));
            }
        });
    }

}
