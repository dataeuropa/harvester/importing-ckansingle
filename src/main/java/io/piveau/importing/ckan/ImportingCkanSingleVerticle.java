package io.piveau.importing.ckan;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.piveau.dcatap.DCATAPUriSchema;
import io.piveau.importing.ckan.response.CkanArrayResult;
import io.piveau.importing.ckan.response.CkanError;
import io.piveau.importing.ckan.response.CkanResponse;
import io.piveau.importing.ckan.utils.SparqlConnector;
import io.piveau.pipe.PipeContext;
import io.piveau.rdf.Piveau;
import io.piveau.vocabularies.Concept;
import io.piveau.vocabularies.CorporateBodies;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.vocabulary.DCTerms;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class ImportingCkanSingleVerticle extends AbstractVerticle {

    public static final String ADDRESS = "io.piveau.pipe.importing.ckansingle.queue";

    private WebClient client;

    private int pageSize;

    private final List<String> ignorePublisher = new ArrayList<>();

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe);
        client = WebClient.create(vertx, new WebClientOptions()
                .setKeepAlive(false));
        ConfigStoreOptions envStoreOptions = new ConfigStoreOptions()
                .setType("env")
                .setConfig(new JsonObject().put("keys", new JsonArray()
                        .add("PIVEAU_PAGE_SIZE")
                        .add("PIVEAU_IGNORE_PUBLISHER")
                        .add("PIVEAU_DCATAP_SCHEMA_CONFIG")));
        ConfigRetriever retriever = ConfigRetriever.create(vertx, new ConfigRetrieverOptions().addStore(envStoreOptions));
        retriever.getConfig()
                .onSuccess(config -> {
                    pageSize = config.getInteger("PIVEAU_PAGE_SIZE", 1000);
                    DCATAPUriSchema.setConfig(new JsonObject().put("baseUri", "http://data.europa.eu/88u/").put("datasetContext", "dataset/"));

                    ignorePublisher.addAll(config.getJsonArray("PIVEAU_IGNORE_PUBLISHER", new JsonArray())
                            .stream().map(Object::toString).toList());

                    startPromise.complete();
                })
                .onFailure(startPromise::fail);
    }

    private void handlePipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();
        pipeContext.log().info("Import started.");

        if (pipeContext.getConfig().containsKey("sparqlEndpoint")) {
            SparqlConnector connector = SparqlConnector.create(client, pipeContext);
            connector.getTotalCount()
                    .onSuccess(totalCount -> queryPage(0, totalCount, Collections.synchronizedMap(new HashMap<>()), pipeContext))
                    .onFailure(pipeContext::setFailure);
        } else {
            fetchPage(0, Collections.synchronizedMap(new HashMap<>()), pipeContext);
        }
    }

    private void fetchPage(int offset, Map<String, List<String>> identifiers, PipeContext pipeContext) {
        String address = pipeContext.getConfig().getString("address");
        client.getAbs(address + "/api/action/package_list")
                .setQueryParam("offset", String.valueOf(offset))
                .setQueryParam("limit", String.valueOf(pageSize))
                .expect(ResponsePredicate.SC_OK)
                .timeout(5000)
                .send()
                .onSuccess(response -> {
                    CkanResponse ckanResponse = new CkanResponse(response.bodyAsJsonObject());
                    if (ckanResponse.isError()) {
                        CkanError ckanError = ckanResponse.getError();
                        pipeContext.setFailure(ckanError.getMessage());
                    } else {
                        CkanArrayResult ckanResult = ckanResponse.getArrayResult();
                        JsonArray result = ckanResult.getContent();
                        if (!result.isEmpty()) {
                            Promise<Void> promise = Promise.promise();
                            fetchDatasets(result, -1, identifiers, pipeContext, promise);
                            promise.future().onComplete(ar -> fetchPage(offset + pageSize, identifiers, pipeContext));
                        } else {
                            pipeContext.log().info("Import metadata finished");
                            if (pipeContext.getConfig().getBoolean("disableDeletion", false)) {
                                identifiers.forEach((catalogue, list) -> {
                                    ObjectNode info = new ObjectMapper().createObjectNode()
                                            .put("content", "identifierList")
                                            .put("catalogue", catalogue);
                                    pipeContext.setResult(new JsonArray(list).encodePrettily(), "application/json", info).forward();
                                });
                            }
                        }
                    }
                })
                .onFailure(pipeContext::setFailure);
    }

    private void queryPage(int offset, int totalCount, Map<String, List<String>> identifiers, PipeContext pipeContext) {
        SparqlConnector connector = SparqlConnector.create(client, pipeContext);
        connector.getIdentifierPage(offset, pageSize)
                .onSuccess(datasets -> {
                    if (!datasets.isEmpty()) {
                        Promise<Void> promise = Promise.promise();
                        List<String> list = datasets.stream().map(uri -> DCATAPUriSchema.parseUriRef(uri).getId()).collect(Collectors.toList());
                        fetchDatasets(new JsonArray(list), totalCount, identifiers, pipeContext, promise);
                        promise.future().onComplete(ar -> queryPage(offset + pageSize, totalCount, identifiers, pipeContext));
                    } else {
                        pipeContext.log().info("Import metadata finished");
                        if (!pipeContext.getConfig().getBoolean("disableDeletion", false)) {
                            identifiers.forEach((catalogue, list) -> {
                                ObjectNode info = new ObjectMapper().createObjectNode()
                                        .put("content", "identifierList")
                                        .put("catalogue", catalogue);
                                pipeContext.setResult(new JsonArray(list).encodePrettily(), "application/json", info).forward();
                            });
                        }
                    }
                })
                .onFailure(pipeContext::setFailure);
    }

    private void fetchDatasets(JsonArray datasets, int total, Map<String, List<String>> identifiers, PipeContext pipeContext, Promise<Void> promise) {
        if (!datasets.isEmpty()) {
            String name = datasets.remove(0).toString();
            JsonObject config = pipeContext.getConfig();
            String address = config.getString("address") + "/dataset/" + name + ".rdf";
            HttpRequest<Buffer> request = client.getAbs(address)
                    .expect(ResponsePredicate.SC_OK)
                    .timeout(5000);
            if (config.containsKey("queryParams")) {
                config.getJsonObject("queryParams", new JsonObject())
                        .forEach(entry -> request.addQueryParam(entry.getKey(), entry.getValue().toString()));
            }
            request.send()
                    .onComplete(ar -> {
                        if (ar.succeeded()) {
                            try {
                                AtomicReference<String> catalogue = new AtomicReference<>(config.getString("catalogue"));
                                Model model = Piveau.toModel(ar.result().body().getBytes(), "application/rdf+xml");
                                model.listObjectsOfProperty(DCTerms.publisher).nextOptional().ifPresentOrElse(node -> {
                                    Concept corporateBody = CorporateBodies.INSTANCE.getConcept(node.asResource());
                                    if (corporateBody != null) {
                                        catalogue.set(corporateBody.getIdentifier().toLowerCase());
                                    } else {
                                        pipeContext.log().warn("Unknown publisher found: {} - {}", name, node.toString());
                                    }
                                }, () -> pipeContext.log().warn("No publisher in {} found", name));

                                if (!ignorePublisher.contains(catalogue.get())) {
                                    identifiers.putIfAbsent(catalogue.get(), new ArrayList<>());
                                    identifiers.get(catalogue.get()).add(name);

                                    ObjectNode dataInfo = new ObjectMapper().createObjectNode()
                                            .put("total", total)
                                            .put("counter", identifiers.get(catalogue.get()).size())
                                            .put("identifier", name)
                                            .put("catalogue", catalogue.get());
                                    String dataset = ar.result().bodyAsString();
                                    String outputFormat = config.getString("outputFormat", "application/rdf+xml");
                                    pipeContext.setResult(dataset, outputFormat, dataInfo).forward();
                                    pipeContext.log().info("Dataset imported: {}", dataInfo);
                                }
                            } catch (Exception e) {
                                pipeContext.log().error("Reading dataset: {}", name, e);
                            }
                        } else {
                            pipeContext.log().error("Fetching dataset {}", name, ar.cause());
                        }
                        fetchDatasets(datasets, total, identifiers, pipeContext, promise);
                    });
        } else {
            promise.complete();
        }
    }

}
