# ChangeLog

## Unreleased

## 3.0.2 (2022-11-21)

**Added:**
* Possibility to configure a list of publisher to ignore

## 3.0.1 (2022-04-25)

**Changed:**
* Updated all authority vocabularies

## 3.0.0 (2021-12-03)

**Changed:**
* Split datasets to publisher catalogues

## 2.0.3 (2021-09-07)

**Removed:**
* Preprocessing of datasets

## 2.0.2 (2021-09-06)

**Added:**
* Query params in pipe config object

**Changed:**
* Collect identifier even if fetching failed

## 2.0.1 (2021-06-23)

**Changed:**
* Connector pipe handling

## 2.0.0 (2021-06-05)

**Changed:**
* Default output format to application/rdf+xml

**Removed:**
* Optional hash calculation

## 1.1.0 (2021-02-01)

**Changed:**
* Switched to Vert.x 4.0.0

## 1.0.5 (2020-06-18)

## 1.0.4 (2020-02-28)

**Changed:**
* Lib update

## 1.0.3 (2019-11-08)

**Added:**
* Configuration `PIVEAU_PAGE_SIZE` from environment

**Fixed:**
* Counting each dataset for dataInfo

## 1.0.2 (2019-10-02)

**Fixed:**
* Sending list of collected identifiers
 
## 1.0.1 (2019-10-02)

**Fixed:**
* Counting all datasets
* Deleting after last page

## 1.0.0 (2019-10-02)

**Added:**
* buildInfo.json for build info via `/health` path
* config.schema.json
* `PIVEAU_LOG_LEVEL` for general configuration of log level for the `io.piveau` package
* `sendHash` for configuring optional hash calculation in pipe
* `sendHash` to config schema
* paging for fetching package list

**Changed:**
* `PIVEAU_` prefix to logstash configuration environment variables
* Upgrade to Vert.x 3.8.1
* Hash is optional and calculation is based on canonical algorithm
* Requires now latest LTS Java 11
* Docker base image to openjdk:11-jre
 
**Fixed:**
* Update all dependencies

## 0.1.0 (2019-05-17)

**Added:**
* `catalogue` read from configuration and pass it to the info object
* Environment `PIVEAU_IMPORTING_SEND_LIST_DELAY` for a configurable delay
* `sendListDelay` pipe configuration option
* `outputFormat`

**Changed:**
* Readme

**Removed:**
* `mode` configuration

## 0.0.1 (2019-05-03)

Initial release
