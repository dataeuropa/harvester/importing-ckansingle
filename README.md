# piveau importing ckan single
Microservice for importing from source and feeding a pipe.

The service is based on the piveau-pipe-connector library. Any configuration applicable for the piveau-pipe-connector can also be used for this service.

## Table of Contents
1. [Build](#build)
1. [Run](#run)
1. [Docker](#docker)
1. [Configuration](#configuration)
    1. [Data Info Object](#data-info-object)
    1. [Environment](#environment)
    1. [Logging](#logging)
    1. [Pipe](#pipe)
1. [License](#license)

## Build
Requirements:
 * Git
 * Maven 3
 * Java 11

```bash
$ git clone <gitrepouri>
$ cd piveau-consus-importing-ckansingle
$ mvn package
```

## Run

```bash
$ java -jar target/importing-ckansingle.jar
```

## Docker

Build docker image:

```bash
$ docker build -t piveau/piveau-consus-importing-ckansingle .
```

Run docker image:

```bash
$ docker run -it -p 8080:8080 piveau/piveau-consus-importing-ckansingle
```
## Configuration

### Environment
See also piveau-pipe-connector

| Env Variable       | Description                                                                     | Default Value |
|:-------------------|:--------------------------------------------------------------------------------|:--------------|
| `PIVEAU_PAGE_SIZE` | The delay in millisecond for sending the identifier list after the last dataset | `1000`        |

### Logging
See [logback](https://logback.qos.ch/documentation.html) documentation for more details

| Env Variable               | Description | Default Value |
|:---------------------------| :--- | :--- |
| `PIVEAU_PIPE_LOG_APPENDER` | Configures the log appender for the pipe context | `STDOUT` |
| `PIVEAU_LOGSTASH_HOST`     | The host of the logstash service | `logstash` |
| `PIVEAU_LOGSTASH_PORT`     | The port the logstash service is running | `5044` |
| `PIVEAU_PIPE_LOG_PATH`     | Path to the file for the file appender | `logs/piveau-pipe.%d{yyyy-MM-dd}.log` |
| `PIVEAU_PIPE_LOG_LEVEL`    | The log level for the pipe context | `INFO` |
| `PIVEAU_LOG_LEVEL`         | The general log level for the `io.piveau` package | `INFO` |

### Pipe

#### Pipe Config Parameters

_mandatory_

* `address`

  Address of the source

* `catalogue`

  The id of the target catalogue

_optional_

* `sparqlEndpoint`

  Optionally use a sparql endpoint for fetching identifiers.

* `disableDeletion`

  Disabling the sending of the list of all identifiers for the deletion phase. Default is `false`

* `qeuryParams`

  A list of query parameters for fetching a dataset. 

* `outputFormat`

  Mimetype to use for payload. Default is `application/rdf+xml`

  Possible output formats are:

    * `application/rdf+xml`
    * `application/n-triples`
    * `application/ld+json`
    * `application/trig`
    * `text/turtle`
    * `text/n3`

#### Pipe Data Info Object

* `total`

  Total number of datasets

* `counter`

  The number of this dataset

* `identifier`

  The unique identifier in the source of this dataset

* `catalogue`

  The id of the target catalogue

* `content`

  The content type of the data object

  Possible values are:

    * `ìdentifierList`

## License

[Apache License, Version 2.0](LICENSE.md)
